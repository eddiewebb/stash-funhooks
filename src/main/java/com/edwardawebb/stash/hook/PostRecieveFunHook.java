package com.edwardawebb.stash.hook;

import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.repository.*;
import com.atlassian.stash.repository.*;
import com.atlassian.stash.setting.*;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.text.StrSubstitutor;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;

public class PostRecieveFunHook implements PreReceiveRepositoryHook, RepositorySettingsValidator
{
    private static Map COLORS  = ImmutableMap.builder()
                                    .put("RED", "\033[0,31m")
                                    .put("GREEN", "\033[0,32m")
                                    .put("YELLOW", "\033[0,33m")
                                    .put("BLUE", "\033[0,34m")
                                    .put("PURPLE", "\033[0,35m")
                                    .put("CYAN", "\033[0,36m")
                                    .put("WHITE", "\033[0,37m")
                                    .put("ORANGE", "\033[38;5;214m")
                                    .put("NOCOLOR", "\033[0m")
                                .build();
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(PostRecieveFunHook.class);




    @Override
    public void validate(Settings settings, SettingsValidationErrors errors, Repository repository)
    {
        if (settings.getString("message", "").isEmpty())
        {
            errors.addFieldError("message", "Make sure to set a message or disable the hook!");
        }
    }

    /**
     * We only use PRE receieve because it gives us access to publish messages back to client
     * @param repositoryHookContext
     * @param collection
     * @param hookResponse
     * @return true, always
     */
    @Override
    public boolean onReceive(RepositoryHookContext repositoryHookContext, Collection<RefChange> collection, HookResponse hookResponse) {
        String message = repositoryHookContext.getSettings().getString("message");
        if (message != null)
        {
            try
            {
                String coloredMessage =  StrSubstitutor.replace(message, COLORS);
                hookResponse.out().printf("%s\n", coloredMessage);
            }
            catch (Exception e)
            {
                //This plugin should never cause a recieve to fail!
                LOG.error("An exception was thrown while trying to format message.");
            }
        }
        return true;
    }
}