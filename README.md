# Silly Post Recieve Hook for Stash (BitBucket Server)
Allows uses to configure custom messages to be returned to users on push.

Includes ability to specify color output.
```${RED}R${ORANGE}A${YELLOW}I${GREEN}N${BLUE}B${CYAN}O${PURPLE}W${NOCOLOR}```

Supported colors can be seen in src/main/java/com/edwardawebb/stash/hook/PostReceiveFunHook, currently:
- "RED"
- "GREEN"
- "YELLOW"
- "BLUE"
- "PURPLE"
- "CYAN"
- "WHITE"
- "ORANGE"
- "NOCOLOR" (resumes normal console scheme)









You have successfully created an Atlassian Plugin!

Here are the SDK commands you'll use immediately:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK
